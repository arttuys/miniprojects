# VocalPiRadio - (c) arttuys 2017, licensed under BSD 3-clause license (https://opensource.org/licenses/BSD-3-Clause) 

require 'rubygems'
require 'bundler/setup' # Set up Bundler dependencies

require 'espeak'
require 'dirble'

# Unfortunately, at the time of writing for this program, there seems to be a major hiccup in terms of how the Dirble API works, and how the Ruby library thinks it should work
# As making a proper patch would take too long for this purpose, we need to implement something commonly called a 'monkey patch'.
# Thanks to Ruby's powerful metaprogramming ability, we can add an additional initialization layer, which then ensures we can access the data we need (but which is not exposed by the default API)


module Dirble
  class Category
    alias old_initialize initialize

    (attr_reader :title) unless method_defined?(:title)

    def initialize(options)
      @title = options[:title]
      old_initialize(options)
    end
  end

  class Station
    alias old_initialize initialize

    (attr_reader :streams) unless method_defined?(:streams)

    def initialize(options)
      @streams = options[:streams]
      old_initialize(options)
    end
  end
end

require 'io/console' # Raw input
require 'continent' # Country data
require 'open3' # Interactions with MPlayer

module VocalPiRadio

  DISPLAY_AVAILABLE = false # This is very useful for debug purposes, as this avoids wasting time for speech
  USE_BELL = true # Set this to false, if you do not desire to use the bell sound

  # Rings a bell sound to indicate that something unusual has occurred
  def self.bell()
    return if (DISPLAY_AVAILABLE || !USE_BELL)
    `mplayer -quiet "#{File.join(File.dirname(__FILE__), "bell.wav")}"` # Trigger mplayer to play the sound; assumed to be in the same folder as the script
  end

  # Speaks out a snippet of text
  #
  # @param [String] text The text to speak out
  # @param [String] lang Language of the text snippet to be spoken; if this voice is not found, English is spoken
  def self.speak(text, lang="en")
    puts "[Speaking '#{text}' in '#{lang}']"
    (ESpeak::Speech.new(text, voice: ESpeak::Voice.all.any? {|v| v.language == lang} ? lang : "en", speed: 165).speak) unless DISPLAY_AVAILABLE
  end

  # A slightly specialized readline for handling numeric/special input. This function returns ONE of follows:
  #
  # 1) an Integer, which describes the selected choice
  # 2) a special character as a single-character string: this may be one of following: + - \e
  # 3) empty string if Enter is simply pressed
  # 3) nil if conflicting input was provided
  def self.number_selective_readline
    num = nil

    loop do
      case STDIN.getch
        when /(\d)/
          num ||= 0
          num = (num*10) + ($1.to_i)
          speak(num.to_s)
        when /\//
          speak("Reset to zero")
          num = 0
        when /[\r\n]/
          return (num != nil ? num : "")
        when /([+\-\e])/
          return (num == nil ? $1 : nil) # Return the special character UNLESS there have been preceding numbers
        when "*"
          return "\e" # For numeric keyboards, use this symbol as the escape character
        else
          return nil
      end
    end

  end

  # A simple selection function; each choice is spoken when selected. One can also press + to hear further information, if available

  # @param [String] intro The question to be asked
  # @param [Array<X>] list The list of items
  # @param [Proc] name_speak_function A function that when presented with an item, should speak its name
  # @param [Boolean] interruptible If TRUE, allow backing out by pressing ESC or *; in this case, NIL is returned
  # @param [Proc] description_speak_function If defined, user can press plus on any choice to speak out a further description
  # @return [X] The chosen item, or NIL if the list is empty, or user interrupted
  def self.select_by_choice(intro, list, name_speak_function, interruptible, description_speak_function=nil)
    return list[0] if ((list.length == 1) && !interruptible) # No choices needed, only one option
    return nil if (list.empty?) # Empty list - return nil

    instruction_text = "#{intro}. Select a number from 1 to #{list.length}, and then press ENTER.#{interruptible ? " Press Escape or * to cancel." : ""}"
    VocalPiRadio.speak(instruction_text) # Speak the instructions

    valid_selection = nil
    loop do
      res = number_selective_readline
      case res
        when "-"
          puts "Received '-' (instructions)"
          VocalPiRadio.speak(instruction_text)
        when Integer
          puts "Received a number"
          if (res > list.length || res < 1) then
            bell
            VocalPiRadio.speak("There are only #{list.length} choices starting from 1, but you chose #{res}. Please try again.")
          else
            # We have a valid selection!
            valid_selection = res
            name_speak_function.call(list[valid_selection-1])
            VocalPiRadio::speak("Press Enter to confirm")
          end
        when "\e"
          puts "Received escape.."
          next unless interruptible
          puts ".. and accepted it"
          return nil
        when "+"
          puts "Received +"
          next if (valid_selection == nil || description_speak_function == nil)
          description_speak_function.call(list[valid_selection-1])
        when ""
          if (valid_selection == nil)
            bell
            VocalPiRadio::speak("You have not made a selection yet. Press MINUS for instructions")
          else
            return list[valid_selection-1]
          end
        when nil
          bell
          VocalPiRadio::speak("That selection was not valid. Try again or press MINUS for instructions")
      end
    end
  end

  # Attempt to actually play the provided station
  # @param [Dirble::Station] station Station to be played
  def self.play_station(station)
    speak("Activating stream.")

    actual_station = Dirble::Station.find(station.id)
    stream_url = actual_station.streams.sample[:stream]

    # Open the stream application.
    Open3.popen2("mplayer -slave -quiet \"#{stream_url}\"") do |stdin, stdout, thread|
      loop do
        begin
          break unless thread.alive?
          char = STDIN.getch
          case char
            when /[\e\r\n]/
              stdin.puts "quit"
              stdin.flush
              break
          end
        rescue Exception => e
          bell
          speak("Error was encountered: #{e.to_s}")
          thread.kill
        end
      end
    end

    speak("That was station ID code #{station.id}")
  end

  # The list of stations
  # @param [Array<Dirble::Station>] list List of stations
  # @param [String] country_hint If these stations are from a certain country, this parameter hints at the pronunciation rules that may apply
  def self.station_list(list, country_hint="en")
    loop do
      station = select_by_choice("Which station would you like to play?", list.select {|station| station.streams != nil && station.streams.length > 0}.sort_by {|station| station.name || "z"}, lambda {|station| VocalPiRadio::speak(station.name, country_hint)}, true, nil)
      return if station == nil # Exit if escaped

      play_station(station) # Play the station
    end
  end

  # The main loop of the radio program. Ask for the type of query, and then pass on the list of stations found
  def self.radio_main
    Dirble.configure do |config|
      config.api_key = File.read(File.join(File.dirname(__FILE__), "API_key.txt")) # Read the API key from the file
    end

    loop do
      choices = {:by_category => "Search by category", :by_country => "Search by country", :by_id => "Select by identifier"}

      choice = select_by_choice("Select the mode.", choices.keys, lambda {|x| VocalPiRadio::speak(choices[x])}, true)

      puts "Selected #{choice}"
      case choice
        when :by_category
          category_list = Dirble::Category.all.select {|x| (x.title != nil && x.title.length > 0) || (x.description != nil && x.description.length > 0)} # Load category data
          ####
          loop do
            # In a loop, select a category and invoke the station selector
            category = select_by_choice("Select the category you would like.",
                                        category_list.sort_by {|catg| catg.name || "z"},
                                        lambda do |catg|
                                          speak(catg.title || catg.description || nil)
                                        end,
                                        true,
                                        lambda {|catg| speak(catg.description) if (catg.description != nil && catg.description.length > 0)})
            break if category == nil # Break out if desired

            stations = category.stations
            if (stations.empty?) then
              bell
              speak("Unfortunately, there are no stations available for this category. Please select another.")
              next
            else
              station_list(stations)
            end
          end
        when :by_country
          speak("Please enter the numeric code of the country, and press ENTER. Press Escape or * to cancel")

          found_choice = nil

          loop do
            # In a loop, figure out the country code
            res = number_selective_readline
            case res
              when Integer
                country = Continent.by_numeric_code(res)
                if (country == nil)
                  bell
                  speak("Invalid country - please try again.")
                else
                  found_choice = country
                  speak("#{country[:name]}. If correct, press Enter, otherwise try again")
                end
              when ""
                if found_choice == nil then
                  bell
                  speak("You have not selected a country yet. Please try again or press MINUS for instructions.")
                  next
                end

                country_code = found_choice[:alpha_2_code].downcase
                stations = Dirble::Station.by_country(country_code)

                if (stations.empty?)
                  bell
                  speak("Unfortunately, there are no stations available for #{found_choice[:name]}. Please select another country.")
                  next
                else
                  station_list(stations, country_code)
                  speak("Please enter another country, or press Escape or * to exit")
                end
              when "-"
                speak("Please enter the numeric code of the country, and press ENTER. Press Escape or * to cancel")
              when "\e"
                break
              else
                bell
                speak("Invalid country code. Please try again, press MINUS for instructions, or press Escape or * to exit")
            end
          end
        when :by_id
          speak("Please enter the identifier of the station to play and then press ENTER, or press Escape or * to cancel")

          found_station = nil

          loop do
            res = number_selective_readline
            case res
              when Integer
                station = Dirble::Station.find(res)
                if (station == nil || station.streams == nil || station.streams.length < 1) then
                  speak("Sorry, #{res} did not match any station")
                else
                  speak("Found station #{station.name}. Press Enter to confirm.")
                  found_station = station
                end
              when "-"
                speak("Please enter the identifier of the station to play, or press Escape or * to cancel")
              when ""
                if (found_station == nil) then
                  speak("You have not selected a station yet.")
                else
                  play_station(found_station)
                  speak("Please select another station, or press Escape or * to cancel")
                end
              when "\e"
                break
              else
                bell
                speak("That was not a valid identifier. Please try again.")
            end
          end
        when nil
          puts "Exiting.."
          return # Exit
      end
    end
  end
end

VocalPiRadio.speak("VocalPiRadio v0.1")
VocalPiRadio.radio_main # Trigger main program
